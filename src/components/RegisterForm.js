import React, {useState} from 'react'
import Paper from '@material-ui/core/Paper';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Axios from 'axios';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';

const RegisterForm = (props) => {
    const {classes, primary} = props;

    const [userFields, setUserFields] = useState({
        email: '',
        password: '',
        confirmPassword: ''
    });

    const handleEmail = (event) => {
        setUserFields({ ...userFields, email: event.target.value });
    };

    const handlePassword = (event) => {
        setUserFields({ ...userFields, password: event.target.value });
    };

    const handleConfirmPassword = (event) => {
        setUserFields({ ...userFields, confirmPassword: event.target.value });
    };


    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <Paper className={classes.paper}>
            <TextField
                variant="outlined"
                type="email"
                value={userFields.email}
                label={'Email'}
                className={classes.textfield}
                color={primary}
                onChange={handleEmail}
            />
            <TextField
                variant="outlined"
                type="password"
                value={userFields.password}
                label={'Password'}
                className={classes.textfield}
                color={primary}
                onChange={handlePassword}
            />
            <TextField
                variant="outlined"
                type="password"
                value={userFields.confirmPassword}
                label={'Confirm Password'}
                className={classes.textfield}
                color={primary}
                onChange={handleConfirmPassword}
            />
            <Button variant="contained" style={{background: primary}} className={classes.button} onClick={async () => {
                const user = {
                    email: userFields.email,
                    password: userFields.password
                };

                if(userFields.password === userFields.confirmPassword) {
                    const response = await Axios.post('/api/users', user);
                    setUserFields({ ...userFields, confirmPassword: '', password: '', email: '' });
                    console.log(response.status);
                    handleClick();
                }
            }}>
                Register
            </Button>
            <Snackbar
                className={classes.success}
                style={{borderRadius: '1px'}}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
            >
                <SnackbarContent
                    className={classes.success}
                    style={{borderRadius: '1px'}}
                    aria-describedby="client-snackbar"
                    message={'You created a user'}
                        action={[
                            <IconButton key="close" aria-label="close" color="inherit" onClick={handleClose}>
                                <CloseIcon className={classes.icon} />
                            </IconButton>,
                        ]}
                    />
            </Snackbar>
        </Paper>
    );
};

export default RegisterForm;