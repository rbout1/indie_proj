import React from 'react';
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Fab from "@material-ui/core/Fab";
import DeleteIcon from '@material-ui/icons/Delete';
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Typography} from "@material-ui/core";
import Axios from 'axios';

const ContactItem = (props) => {
    const {contacts, handleDeleteContact, primary} = props;

    const classes = makeStyles(theme => ({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
        fab: {
            margin: theme.spacing(1),
            display: 'inline-block',
        },
    }));

    return (
        <Grid container spacing={0}>
            {contacts.slice(0).reverse().map(contact => {
                return <Grid item xs={12} key={contact.email} style={{ marginBottom: '1rem' }}>
                    <Paper className={classes.paper} style={{ padding: '1rem', width: '58vw', top: 0, bottom: 0, left: 0, right: 0, margin: 'auto'}}>
                        <div style={{ display: 'flex', alignItems: 'center', marginBottom: '1rem' }}>

                            <div style={{ width: '15rem'}}>
                                <Typography component="h4">
                                    <h4>{contact.name}</h4>
                                </Typography>
                                <Typography component="p">
                                    <p>{contact.email}</p>
                                </Typography>
                            </div>
                            <div style={{width: '100%', textAlign: 'right'}}>
                                <Fab aria-label="delete" className={classes.fab} style={{background: primary}} onClick={async () => {
                                    handleDeleteContact(contact.email);
                                    const response = await Axios.delete('/api/contacts/' + contact.email);
                                }}>
                                    <DeleteIcon />
                                </Fab>
                            </div>
                        </div>
                    </Paper>
                </Grid>
            })}
        </Grid>
    )
};

export default ContactItem;