import React from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import { makeStyles } from "@material-ui/core";
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CardActions from '@material-ui/core/CardActions';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import clsx from 'clsx';
import IconButton from "@material-ui/core/IconButton";
import Collapse from '@material-ui/core/Collapse';

const useStyles = makeStyles(theme => ({
    card: {
        width: '20em',
        marginBottom: '5em',
        display: 'inline-block',
    },
    header: {
        fontSize: 14,
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: '41%',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
}));

const InfoCard = (props) => {

    const classes = useStyles();

    const { title, content, primary, loginClick, type } = props;

    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    let pic = <div></div>

    if(type === 'login') {
        pic = <i className="fas fa-sign-in-alt fa-4x"></i>
    }

    return (
        <Card className={classes.card}>
            <CardHeader title={title} style={{textAlign: 'center'}}/>
            {pic}
            <CardActions disableSpacing>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: expanded,
                    })}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography>
                        {content}
                    </Typography>
                </CardContent>
                <CardActions disableSpacing>
                    <Button variant="contained" style={{background: primary}} onClick={loginClick}>Go</Button>
                </CardActions>
            </Collapse>
        </Card>
    );
};

export default InfoCard;