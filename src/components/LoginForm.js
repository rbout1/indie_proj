import React, {useState} from 'react';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Axios from "axios";

function LoginForm ({classes, primary, history, handleLogin}) {
    const [userFields, setUserFields] = useState({
        email: '',
        password: '',
        confirmPassword: ''
    });

    const handleEmail = (event) => {
        setUserFields({...userFields, email: event.target.value});
    };

    const handlePassword = (event) => {
        setUserFields({...userFields, password: event.target.value});
    };

    return (
        <Paper className={classes.paper}>
            <TextField
                variant="outlined"
                onChange={handleEmail}
                value={userFields.email}
                type="email"
                label={'Email'}
                className={classes.textfield}
                color={primary}
                style={{marginTop: '33%'}}
            />
            <TextField
                variant="outlined"
                label={'Password'}
                value={userFields.password}
                onChange={handlePassword}
                type={'password'}
                className={classes.textfield}
                color={primary}
            />
            <Button variant="contained" style={{background: primary}} className={classes.button}
                    onClick={async () => {
                        const user = {
                            email: userFields.email,
                            password: userFields.password
                        };

                        const response = await Axios.post('/api/isValid', user);
                        if (response.status === 200) {
                            setUserFields({...userFields, password: '', email: ''});
                            const contacts = await Axios.get('/api/contacts/' + user.email);
                            history.push('/contacts');
                            handleLogin(user, contacts.data);
                        }
                    }}>
                Login
            </Button>
        </Paper>
    );
};

export default LoginForm;