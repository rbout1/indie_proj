import React, {useState} from 'react';
import Paper from "@material-ui/core/Paper";
import {makeStyles} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Axios from "axios";
import ContactItem from '../components/ContactItem';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import TextField from "@material-ui/core/TextField";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";

const useStyles = makeStyles(theme => ({
    paper: {
        margin: 'auto',
        width: '60vw',
        height: '25vh',
        marginBottom: '1em',
        marginTop: '1em',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
    },
    fab: {
        position: 'absolute',
        bottom: theme.spacing(4),
        right: theme.spacing(4),
    },
    cancelButton: {
        left: theme.spacing(1),
        position: 'absolute',
    },
    nameDiv: {
        float: 'right',
        margin: 15,
    },
    button: {
        margin: '1em'
    }
}));

function Contacts ({history, isLoggedIn, email, primary, contacts, handleAddContact, handleDeleteContact, handleLogout}) {
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const [userFields, setUserFields] = useState({
        email: '',
        firstName: '',
        lastName: ''
    });

    const handleEmailChange = (event) => {
        setUserFields({...userFields, email: event.target.value});
    };

    const handleFirstNameChange = (event) => {
        setUserFields({...userFields, firstName: event.target.value});
    };

    const handleLastNameChange = (event) => {
        setUserFields({...userFields, lastName: event.target.value});
    };

    const classes = useStyles();

    let element;

    if(isLoggedIn) {
        element = <div>
            <div>
                <Paper className={classes.paper}>
                    <div style={{padding: '2em'}}>
                        <Typography component="p">
                            You are logged in and if you click this delete button you can delete this account. If you use the
                            add button in the bottom right you can create a contact and add it to a list. This list is a separate
                            database and is only unique to your account. Since this is a demo you can create fake contacts.
                            These contacts can be deleted.
                        </Typography>
                        <Button variant="contained" style={{background: primary}} className={classes.button} onClick={async () => {
                            await Axios.delete('/api/users/' + email);
                            history.push('/logindemo')
                        }}>
                            Delete
                        </Button>
                        <Button variant={'contained'} style={{background: primary}} className={classes.button} onClick={() => {
                            handleLogout();
                            history.push('/logindemo')
                        }}>
                            Logout
                        </Button>
                    </div>
                </Paper>
                <ContactItem contacts={contacts}
                             handleDeleteContact={handleDeleteContact}
                             primary={primary}
                />
            </div>
            <Fab className={classes.fab} style={{background: primary}} onClick={handleClickOpen}>
                <AddIcon />
            </Fab>
            <Dialog open={open} onClose={handleClose}>
                <DialogActions>
                    <Button onClick={handleClose} className={classes.cancelButton}>
                        Cancel
                    </Button>
                    <Button onClick={async () => {
                        setOpen(false);

                        const contact = {
                            creatorEmail: email,
                            email: userFields.email,
                            name: userFields.firstName + ' ' + userFields.lastName
                        };

                        await Axios.post('/api/contacts', contact);
                        const contacts = await Axios.get('/api/contacts/' + email);
                        handleAddContact(contacts.data);
                    }}>
                        Save
                    </Button>
                </DialogActions>
                <DialogContent>
                    <DialogContentText>
                        Add new contact
                    </DialogContentText>
                    <div className={classes.nameDiv}>
                        <TextField
                            margin="dense"
                            id="first-name"
                            label="First Name"
                            color={"secondary"}
                            onChange={handleFirstNameChange}
                        />
                        <br/>
                        <TextField
                            margin="dense"
                            id="last-name"
                            label="Last Name"
                            color={"secondary"}
                            onChange={handleLastNameChange}
                        />
                    </div>
                    <br/>
                    <TextField
                        margin="normal"
                        id="email"
                        label="Email Address"
                        type="email"
                        color={"secondary"}
                        onChange={handleEmailChange}
                        fullWidth={true}
                    />
                </DialogContent>
            </Dialog>
        </div>
    } else {
        element = <Paper className={classes.paper}>
            <div style={{padding: '2em'}}>
                <Typography component="p">
                    You are not logged in
                </Typography>
            </div>
        </Paper>
    }

    return (
        <div>{element}</div>
    )
};

export default Contacts;