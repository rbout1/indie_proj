import React from 'react';
import {makeStyles} from "@material-ui/core";
import Typography from '@material-ui/core/Typography';
import LoginForm from '../components/LoginForm';
import RegisterForm from "../components/RegisterForm";
import Paper from "@material-ui/core/Paper";
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    paper: {
        width: '15em',
        height: '100%',
        margin: '1em',
        display: 'inline-block',
        verticalAlign: 'top',
        textAlign: 'left'
    },
    textfield: {
        margin: '0.5em',
        width: '93%'
    },
    button: {
        margin: '0.5em',
        bottom: 0,
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    success: {
        backgroundColor: green[600],
    }
}));

const LoginDemo = (props) => {
    const classes = useStyles();

    const {primary, history, handleLogin, getSession} = props;

    return (
        <div style={{height: '17em', marginTop: '10%', textAlign: 'center'}}>
            <Paper className={classes.paper}>
                <Typography component="p" style={{padding: '0.5em', textAlign: 'left'}}>
                    You'll need to register before you are able to login unless you have already created an account.
                    You can use a fake email, I'm not sending an email to verify it.
                </Typography>
                <Button variant="contained" style={{background: primary, marginTop: '26%'}} className={classes.button}
                    onClick={() => {
                        history.push('/');
                }}>
                    Home
                </Button>
            </Paper>
            <RegisterForm
                classes={classes}
                primary={primary}
            />
            <LoginForm
                classes={classes}
                primary={primary}
                history={history}
                handleLogin={handleLogin}
                getSession={getSession}
            />
        </div>
    );
};

export default LoginDemo;