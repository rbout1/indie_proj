import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import InfoCard from '../components/InfoCard';

const useStyles = makeStyles(theme => ({
    paper: {
        margin: 'auto',
        marginTop: '1em',
        width: '42.5vw',
        height: '43em',
    },
    root: {
        textAlign: 'center',
        width: '100vw',
    },
    img: {
        float: 'right',
        width: '50%',
        height: '100%',
        borderRadius: '0px 3px 3px 0px'
    },
    fab: {
        bottom: 0,
        marginBottom: '1em',
        position: 'absolute',
        left: '50%'
    },
    footer: {
        display: 'block',
        textAlign: 'center',
        color: '#9e9e9e',
        margin: '2em',
        bottom: 0,
        width: '100%',
        marginLeft: '0em',
    },
    icon: {
        margin: '0.5em',
        display: 'inline-block'
    }
}));

const Home = (props) => {
    const classes = useStyles();

    const {primary, history} = props;

    const loginClick = () => {
        history.push('/logindemo');
    };

    return (
        <div>
            <Box className={classes.root}>
                <Paper className={classes.paper}>
                        <div style={{padding: '2em', maxHeight: '500px', textAlign: 'left'}}>
                            <Typography component="h1">
                                <h1>
                                    Tools I am using to build this website: <br />
                                </h1>
                            </Typography>
                            <Typography component={"p"}>
                                <div className={classes.icon}>
                                    <i className="fab fa-react fa-7x"></i>
                                    <h2 style={{display: 'inline', margin: '1em'}}>
                                        React
                                    </h2>
                                </div>
                                <br />
                                <div className={classes.icon}>
                                    <i className="fab fa-js fa-7x"></i>
                                    <h2 style={{display: 'inline', margin: '1em'}}>
                                        Javascript
                                    </h2>
                                </div>
                                <br />
                                <div className={classes.icon}>
                                    <i className="fab fa-node-js fa-7x"></i>
                                    <h2 style={{display: 'inline', margin: '1em'}}>
                                        Node.js
                                    </h2>
                                </div>
                                <br />
                                <div className={classes.icon}>
                                    <i className="fas fa-database fa-7x"></i>
                                    <h2 style={{display: 'inline', margin: '1em'}}>
                                    MongoDB
                                    </h2>
                                </div>
                            </Typography>
                        </div>
                </Paper>
            </Box>
            <div style={{textAlign: 'center', marginTop: '3em'}}>
                <InfoCard
                    title={'Login/Register Demo'}
                    content={'A simple login and register demo that uses a Node.JS server and a MongoDB' +
                    ' database to store the info.'}
                    primary={primary}
                    loginClick={loginClick}
                    type={'login'}
                />
            </div>
            <footer className={classes.footer}>
                <hr style={{borderColor: '#9e9e9e'}}/>
                <Typography component={'p'}>
                    Made with <i className="fas fa-heart"></i> by Robert Boutillier
                </Typography>
            </footer>
        </div>
    );
};

export default Home;