import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Home from './pages/Home';
import LoginDemo from './pages/LoginDemo';
import Contacts from "./pages/Contacts";

const primary = 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)';

const theme = createMuiTheme({
    palette: {
        type: 'dark',
    },
});

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            email: '',
            contacts: []
        };
    }
    handleLogin = (user, contacts) => {
        this.setState({isLoggedIn: true});
        this.setState({email: user.email});
        this.setState({contacts: contacts});
    };

    handleAddContact = (contacts) => {
        this.setState({contacts: contacts});
    };

    handleDeleteContact = (email) => {
        let newContacts = this.state.contacts;
        let i;
        for(i = 0; i<newContacts.length; i++) {
            if (newContacts[i].email === email) {
                newContacts.splice(i, 1);
            }
        }
        this.setState({contacts: newContacts});
    };

    handleLogout = () => {
        this.setState({isLoggedIn: false});
        this.setState({email: ''});
        this.setState({contacts: []});
    };

    render() {
        return (
            <ThemeProvider theme={theme}>
                <Router>
                    <Route exact={true} path={'/'}
                           render={({history}) => (<Home primary={primary} history={history}/>)}/>
                    <Route exact={true} path={'/logindemo'}
                           render={({history}) => (<LoginDemo primary={primary}
                                                              history={history}
                                                              handleLogin={this.handleLogin}
                           />)}/>
                    <Route exact={true} path={'/contacts'}
                           render={({history}) => (<Contacts history={history}
                                                             isLoggedIn={this.state.isLoggedIn}
                                                             email={this.state.email}
                                                             primary={primary}
                                                             contacts={this.state.contacts}
                                                             handleAddContact={this.handleAddContact}
                                                             handleDeleteContact={this.handleDeleteContact}
                                                             handleLogout={this.handleLogout}
                           />)}/>
                </Router>
            </ThemeProvider>
        );
    }
};

export default App;
